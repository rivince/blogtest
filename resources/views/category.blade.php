@extends('layouts.app')


@section('content')
<section class="blog bgwhite p-t-94 p-b-65">
		<div class="container">
			<div class="sec-title p-b-52">
				<h3 class="m-text5 t-center">
					 Manage Category
				</h3>
			</div>

			<div class="col-md-12 p-b-50">
				<form action="{{  route('category') }}" method="post">
					@csrf

					@if (session('success'))
						<h4 class="m-text26 p-b-36 p-t-12" style="color:green;">
							{{ session('success')}}
						</h4>

					@endif

					<label for="categorytitle">Category</label>
					@error('categorytitle')
						<label style="color:red; font-size: 11px;"> {{ $message }} </label>
					@enderror
					<div class="bo4 of-hidden size15 m-b-20">
						
						<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="categorytitle" id ="categorytitle" placeholder="Category" value="{{ old('categorytitle') }}">
					</div>


					<div class="w-size25">
						<!-- Button -->
						<button class="flex-c-m size2 bg1 bo-rad-23 hov1 m-text3 trans-0-4">
							Submit
						</button>
					</div>
				</form>

				

			</div>

			<div class="col-md-12 p-b-50">
				<div class="sec-title p-b-52">
					<h3 class="m-text5 t-center">
						 Category List
					</h3>
				</div>


					<div class="col-md-12 p-b-50">

						<table id="categorytable" class="table table-bordered table-hover">
				                <thead>
				                <tr>
				                  <th>Categories</th>
				                  <th>Action</th>
				                </tr>
				                </thead>
				                <tbody>


				                 @if($categories->count())

							 		@foreach ($categories as $category)
				                          <tr>
						                    <td>{{$category->categorytitle}}</td>
						                    <td style="width: 20%;">
													<form action="{{ route('category.delete', $category) }}" method= "post">
							                    		<a href="{{ route('editcategory', $category->id) }}" class="btn btn-success"><i class="fa fa-edit"></i> </a>
							                    		@method("DELETE")
							                    		@csrf
							                    		
							                    		<button type="submit" class="btn btn-info"><i class="fa fa-times"></i> </button> 

							                    	</form>
						                    </td>
						                  </tr>
			                        @endforeach
			                     @else

			                     @endif
				              </table>
				              {{ $categories->links() }}

					</div>

				</div>


		</div>
</section>


@endsection