@extends('layouts.app')


@section('content')
<section class="blog bgwhite p-t-94 p-b-65">
		<div class="container">
			<div class="sec-title p-b-52">
				<h3 class="m-text5 t-center">
					 Create Blog
				</h3>
			</div>

			<div class="col-md-12 p-b-50">
				<form action="{{  route('dashboard') }}" method="post">
					@csrf

					@if (session('success'))
						<h4 class="m-text26 p-b-36 p-t-12" style="color:green;">
							{{ session('success')}}
						</h4>

					@endif

					<label for="category">Category</label>
					@error('category')
						<label style="color:red; font-size: 11px;"> {{ $message }} </label>
					@enderror
					<div class="bo4 of-hidden size15 m-b-20">
						
						 <select class="sizefull s-text7 p-l-22 p-r-22" id="category" name ="category">
						 	@if($categories->count())

						 		@foreach ($categories as $category)
			                        <option value='{{$category->id}}'>{{$category->categorytitle}}</option>
		                        @endforeach
		                     @else

		                     @endif
	                      </select>

					</div>
					

					<label for="title">Title</label>
					@error('title')
						<label style="color:red; font-size: 11px;"> {{ $message }} </label>
					@enderror
					<div class="bo4 of-hidden size15 m-b-20">
						
						<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="title" id ="title" placeholder="Title" value="{{ old('title') }}">
					</div>

					<label for="content">Content</label>
					@error('content')
						<label style="color:red; font-size: 11px;"> {{ $message }} </label>
					@enderror
					
						
						<textarea class="dis-block s-text7 size20 bo4 p-l-22 p-r-22 p-t-13 m-b-20" name="content" id = "content"   placeholder="Content"> {{ old('content') }}</textarea>
					


					<div class="w-size25">
						<!-- Button -->
						<button class="flex-c-m size2 bg1 bo-rad-23 hov1 m-text3 trans-0-4">
							Post
						</button>
					</div>
				</form>

				

			</div>


		</div>
</section>


@endsection