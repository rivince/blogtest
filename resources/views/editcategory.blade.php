@extends('layouts.app')


@section('content')
<section class="blog bgwhite p-t-94 p-b-65">
		<div class="container">
			<div class="sec-title p-b-52">
				<h3 class="m-text5 t-center">
					 Update Category
				</h3>
			</div>

			<div class="col-md-12 p-b-50">
				<form action="/editcategory" method="post">
					<input type="hidden" name="id" id ="id" placeholder="Category" value="{{ $categories->id }}">
					@csrf

					@if (session('success'))
						<h4 class="m-text26 p-b-36 p-t-12" style="color:green;">
							{{ session('success')}}
						</h4>

					@endif

					<label for="categorytitle">Category</label>
					@error('categorytitle')
						<label style="color:red; font-size: 11px;"> {{ $message }} </label>
					@enderror
					<div class="bo4 of-hidden size15 m-b-20">
						
						<input class="sizefull s-text7 p-l-22 p-r-22" type="text" name="categorytitle" id ="categorytitle" placeholder="Category" value="{{ $categories->categorytitle }}">
					</div>


					<div class="w-size25">
						<!-- Button -->
						<button class="flex-c-m size2 bg1 bo-rad-23 hov1 m-text3 trans-0-4">
							Submit
						</button>
					</div>
				</form>

				

			</div>

		</div>
</section>


@endsection