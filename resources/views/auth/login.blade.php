@extends('layouts.app')

@section('content')


<section class="blog bgwhite p-t-94 p-b-65">
		<div class="container">
			<div class="sec-title p-b-52">
				<h3 class="m-text5 t-center">
					 Login
				</h3>
			</div>

			<div class="col-md-12 p-b-30">
				<form action="{{  route('login') }}" method="post">
					@csrf
					@if (session('status'))
						<h4 class="m-text26 p-b-36 p-t-12" style="color:red;">
							{{ session('status')}}
						</h4>

					@endif


					<label for="email">Email</label>
					@error('email')
						<label style="color:red; font-size: 11px;"> {{ $message }} </label>
					@enderror
					<div class="bo4 of-hidden size15 m-b-20">
						
						<input class="sizefull s-text7 p-l-22 p-r-22" type="email" name="email" id ="email" placeholder="Email" value="{{ old('email') }}"> 

					</div>
					


					<label for="password">Password</label>
					@error('password')
						<label style="color:red; font-size: 11px;"> {{ $message }} </label>
					@enderror
					<div class="bo4 of-hidden size15 m-b-20">
						
						<input class="sizefull s-text7 p-l-22 p-r-22" type="password" name="password" id="password" placeholder="Password" >
					</div>


					<div class="w-size25">
						<!-- Button -->
						<button class="flex-c-m size2 bg1 bo-rad-23 hov1 m-text3 trans-0-4">
							Login
						</button>
					</div>
				</form>
			</div>
		</div>
</section>
	

@endsection

