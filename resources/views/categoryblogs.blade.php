@extends('layouts.app')

@section('content')

<section class="blog bgwhite p-t-94 p-b-65">


		<div class="container">
			<div class="sec-title p-b-52">
				<h3 class="m-text5 t-center">
					 {{ $category->categorytitle }}
				</h3>
			</div>

			<div class="row">

				@if ($blogs->count())
					@foreach ($blogs as $blog)
						<div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
					<!-- Block3 -->
							<div class="block3">
								<div class="block3 bg6 p-t-30 p-b-30 p-l-30 p-r-30">
									<h4 class="p-b-7">
										<a href="{{ route('blog',$blog->id) }}" class="m-text4">
											{{ Str::limit($blog->title, 30) }}
										</a>
									</h4>
									<span class="s-text6"><a href=""> <strong> {{$blog->category->categorytitle}} </strong></a></span>
									<span class="s-text6">By</span> <span class="s-text7"><a href ="{{ route('users.blogs', $blog->user) }}" >{{ $blog->user->name }}</a></span>
									<span class="s-text6">on</span> <span class="s-text7">{{ $blog->created_at->diffforhumans() }}</span>

									<p class="s-text8 p-t-16">
										{{ Str::limit($blog->content, 250) }}
									</p>
								</div>
							</div>
						@auth
							<div class="col-sm-10  col-md-4 p-b-30  m-l-r-auto">
								@if($blog->owned(auth()->user()))
									<form action="{{ route('blog.delete', $blog) }}" method="post">
										@method("DELETE")
										@csrf
										<button class="btn btn-info">
											<i class="fa fa-times"> </i>
										</button>
										
										<a href="" class="btn btn-success">
											<i class="fa fa-edit"> </i>
										</a>
									</form>
								@endif
							</div>
						@endauth


						</div>
						

					@endforeach
				 
				@else
						<div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">

							<p class="s-text8 p-t-16">
								No Available Posts.
							</p>
						</div>
				@endif
				
			</div>
			<div class="col-sm-10 col-md-4 p-b-30 m-l-r-auto">
					{{ $blogs->links() }}
				</div>
			
		</div>
</section>


@endsection