@extends('layouts.app')

@section('content')

<section class="blog bgwhite p-t-94 p-b-65">


		<div class="container">
			<div class="sec-title p-b-52">
				<h3 class="m-text5 t-center">
					 {{ $blogs->title }}
				</h3>
				<div class="block3-txt p-t-14 t-center">
							<span class="s-text6"><a href="{{ route('categoryblogs.blogs', $blogs->category) }}"> <strong> {{$blogs->category->categorytitle}} </strong></a></span>
							<span class="s-text6">By</span> <span class="s-text7"><a href ="{{ route('users.blogs', $blogs->user) }}" >{{ $blogs->user->name }}</a></span>
							<span class="s-text6">on</span> <span class="s-text7">{{ $blogs->created_at->diffforhumans() }}</span>

						</div>
			</div>

			<div class="row">

				<div class="col-sm-10 col-md-10 p-b-30 m-l-r-auto">
			<!-- Block3 -->
					<div class="block3">
						<div class="block3-txt p-t-14">

							<p class="s-text8 p-t-16">
								{{ $blogs->content }}
							</p>
						</div>
					</div>
				</div>
						
				
			</div>
		</div>
	</section>


@endsection