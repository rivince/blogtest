<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Blog;

class Category extends Model
{
    use HasFactory;

    protected $fillable = [
        'categorytitle',
    ];

    public function owned(User $user)
    {
    	return $user->id === $this->user_id;
    }

    public function blogs()
    {
        return $this->hasMany(Blog::class);
    }
}
