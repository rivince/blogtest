<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;
use App\Models\Category;

class Blog extends Model
{
    use HasFactory;

     protected $fillable = [
        'title',
        'content',
        'category_id',
    ];

     public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function owned(User $user)
    {
        return $user->id === $this->user_id;
    }
}
