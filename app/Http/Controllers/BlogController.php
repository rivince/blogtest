<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Blog;
use App\Models\Category;

class BlogController extends Controller
{



    public function index()
    {
    	$blogs = Blog::simplepaginate(6);
    	return view('index',['blogs' => $blogs]);
    }


     public function delete(Blog $blog)
    {
    	$blog->delete();
    	return back();
    }

    public function ShowBlog($id)
    {
    	$this->middleware(['auth']);
    	
    	$blogs = Blog::find($id);
    	$categories = Category::get();
    	return view('editblog',['blogs' => $blogs,'categories' => $categories]);
    }


    public function UpdateBlog(Request $request)
    {
    	$blogs = Blog::find($request->id);

    	$blogs->title =  $request->title;
    	$blogs->content =  $request->content;
    	$blogs->category_id =  $request->category;
    	$blogs->save();

    	return redirect()->route('index')->with('success','Updated Successfully!');
    }

    public function DisplayBlog($id)
    {
    	$blogs = Blog::find($id);
    	return view('blog',['blogs' => $blogs]);
    }


}
