<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;


class CategoryController extends Controller
{
	public function __construct()
    {
        $this->middleware(['auth']);
    }
    public function index()
    {
    	$categories = Category::orderBy('categorytitle','asc')->simplepaginate(10);
    	return view('category',['categories' => $categories]);
    }

    public function save(Request $request)
    {
    	$this->validate($request, [
    		'categorytitle' => 'required',

    	]);

    	
    	Category::create([
    		'categorytitle' => $request->categorytitle,

    	]);

    	return redirect()->route('category')->with('success','Saved Successfully!');
    }

    public function delete(Category $category)
    {
    	$category->delete();
    	return back();
    }

       public function ShowCategory($id)
    {
    	$categories = Category::find($id);
    	return view('editcategory',['categories' => $categories]);
    }

    public function UpdateCategory(Request $request)
    {
    	$category = Category::find($request->id);

    	$category->categorytitle =  $request->categorytitle;
    	$category->save();

    	return redirect()->route('category')->with('success','Updated Successfully!');
    }

}
