<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
class CategoryblogsController extends Controller
{
    public function index(Category $category)
    {

		$blogs = $category->blogs()->simplepaginate(6);
    	return view('categoryblogs',[ 'category' => $category, 'blogs' => $blogs]) ;
    }
}
