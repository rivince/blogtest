<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Category;
use App\Models\Blog;


class DashboardController extends Controller
{

    public function __construct()
    {
        $this->middleware(['auth']);
    }
    public function index()
    {
    	$categories = Category::get();
    	return view('dashboard',['categories' => $categories]);
    }

     public function post(Request $request)
    {
    	$this->validate($request, [
    		'category' => 'required',
    		'title' => 'required|max:225',
    		'content' => 'required',

    	]);

    	
    	$request->user()->blogs()->create([
    		'category_id' => $request->category,
    		'title' => $request->title,
    		'content' => $request->content,

    	]);

    	return redirect()->route('dashboard')->with('success','Posted Successfully!');
    }
}
