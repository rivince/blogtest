<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;

class UserBlogsController extends Controller
{
    public function index(User $user)
    {

		$blogs = $user->blogs()->simplepaginate(6);
    	return view('users',[ 'user' => $user, 'blogs' => $blogs]) ;
    }
}
