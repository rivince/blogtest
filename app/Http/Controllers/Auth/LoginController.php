<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class LoginController extends Controller
{

    public function __construct()
    {
        $this->middleware(['guest']);
    }

    public function index()
    {
    	return view('auth.login');
    }


    public function login(Request $request)
    {
    	$this->validate($request, [
    		'email' => 'required',
    		'password' => 'required',

    	]);

    	$credentials = ['email' => $request->email, 'password' => $request->password];

    	if(!Auth::guard('web')->attempt($credentials, false, false)){
    		return back()->with('status','Invalid email or password!');
    	}

    	return redirect()->route('dashboard');
    }
}
