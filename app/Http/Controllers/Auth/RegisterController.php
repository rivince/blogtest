<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;
use App\Models\User;

class RegisterController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware(['guest']);
    }

    public function index()
    {
    	return view('auth.register');
    }

     public function register(Request $request)
    {
    	$this->validate($request, [
    		'email' => 'required|max:225',
    		'name' => 'required|max:225',
    		'password' => 'required|confirmed',

    	]);

    	User::create([
    		'email' => $request->email,
    		'name' => $request->name,
    		'password' => Hash::make($request->password),

    	]);

    	$credentials = ['email' => $request->email, 'password' => $request->password];

    	Auth::guard('web')->attempt($credentials, false, false);

    	return redirect()->route('dashboard');
    }

  
}
