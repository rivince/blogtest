<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\BlogController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\UserBlogsController;
use App\Http\Controllers\CategoryblogsController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['middleware' => 'auth',], function () {
   Route::get('/editblog/{blog}', [BlogController::class, 'ShowBlog'])->name('editblog');
	Route::post('/editblog', [BlogController::class, 'UpdateBlog']);

	Route::get('/category', [CategoryController::class, 'index'])->name('category');
	Route::post('/category', [CategoryController::class, 'save']);
	Route::delete('/category/{category}', [CategoryController::class, 'delete'])->name('category.delete');;
	Route::get('/categoryblogs/{category:categorytitle}/blogs', [CategoryblogsController::class, 'index'])->name('categoryblogs.blogs');
	Route::get('/editcategory/{category}', [CategoryController::class, 'ShowCategory'])->name('editcategory');
	Route::post('/editcategory', [CategoryController::class, 'UpdateCategory']);
	Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
	Route::post('/dashboard', [DashboardController::class, 'post']);

});


Route::get('/register', [RegisterController::class, 'index'])->name('register');
Route::post('/register', [RegisterController::class,'register']);


Route::get('/login', [LoginController::class, 'index'])->name('login');
Route::post('/login', [LoginController::class,'login']);

Route::get('/logout', [LogoutController::class, 'logout'])->name('logout');

Route::get('/post', [DashboardController::class, 'post'])->name('post');



Route::get('/users/{user}/blogs', [UserBlogsController::class, 'index'])->name('users.blogs');



Route::get('/', [BlogController::class,'index'])->name('index');
Route::delete('/{blog}', [BlogController::class, 'delete'])->name('blog.delete');


Route::get('/blog/{blog}', [BlogController::class, 'DisplayBlog'])->name('blog');

